package emc.marketplace.main;

import emc.marketplace.gui.ModListScreen;
import emc.marketplace.modinstaller.API;
import me.deftware.client.framework.main.EMCMod;
import me.deftware.client.framework.wrappers.IMinecraft;
import me.deftware.client.framework.wrappers.gui.IGuiScreen;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends EMCMod {

    public static final Logger logger = LogManager.getLogger("EMC-Marketplace");
    public static String status = "";
    public static int version;
    public static Main INSTANCE;

    @Override
    public void initialize() {
        INSTANCE = this;
        version = modInfo.get("version").getAsInt();
        new EventManager(this);
        API.removeDeprecated();
        new Thread(() -> {
            try {
                Thread.currentThread().setName("Mod fetcher thread");
                API.fetchMods((mods) -> {
                    logger.info("Fetched mods");
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }).start();
    }

    @Override
    public void callMethod(String method, String caller, Object parent) {
        if (method.equals("openGUI()")) {
            IMinecraft.setGuiScreen(new ModListScreen(this, (IGuiScreen) parent));
        } else if (method.startsWith("setStatus(")) {
            status = method.substring("setStatus(".length(), method.length() - 1);
        }
    }

}
