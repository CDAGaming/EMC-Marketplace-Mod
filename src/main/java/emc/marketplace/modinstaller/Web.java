package emc.marketplace.modinstaller;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

public class Web {

	static String get(String uri, String[] headers) throws Exception {
		URL url = new URL(uri);
		Object connection = (uri.startsWith("https://") ? (HttpsURLConnection) url.openConnection()
				: (HttpURLConnection) url.openConnection());
		((URLConnection) connection).setConnectTimeout(8 * 1000);
		((URLConnection) connection).setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
		if (headers != null) {
			for (String header : headers) {
				String key = header.split(": ")[0],
						value = header.split(": ")[1];
				((URLConnection) connection).setRequestProperty(key, value);
			}
		}
		((HttpURLConnection) connection).setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(((URLConnection) connection).getInputStream()));
		String text;
		String result = "";
		while ((text = in.readLine()) != null) {
			result = result + text;
		}
		in.close();
		return result;
	}

	static void downloadMod(String uri, String fileName) throws Exception {
		URL url = new URL(uri);
		Object connection = (uri.startsWith("https://") ? (HttpsURLConnection) url.openConnection()
				: (HttpURLConnection) url.openConnection());
		((URLConnection) connection).setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
		((HttpURLConnection) connection).setRequestMethod("GET");
		FileOutputStream out = new FileOutputStream(fileName);
		InputStream in = ((URLConnection) connection).getInputStream();
		int read;
		byte[] buffer = new byte[4096];
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
		in.close();
		out.close();
	}

}
