package emc.marketplace.modinstaller;


import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import emc.marketplace.main.Main;
import lombok.Getter;
import me.deftware.client.framework.path.OSUtils;
import me.deftware.client.framework.wrappers.IMinecraft;
import org.apache.commons.io.FileUtils;

import javax.annotation.Nullable;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

public class API {

    private static final @Getter String endpoint = "https://gitlab.com/EMC-Framework/maven/raw/master/marketplace/index.json";

	private static @Getter ArrayList<String> verifiedMods = new ArrayList<>();

	@Nullable
	private static @Getter HashMap<String, Mod> cachedMods;

	@Nullable
	public static Mod getMod(String id) {
		if (cachedMods == null || cachedMods.size() == 0) {
			return null;
		}
		return cachedMods.getOrDefault(id, null);
	}

	public static void fetchMods(Consumer<List<Mod>> callback) {
		Main.logger.info("Fetching mods...");
		try {
			cachedMods = new HashMap<>();
			JsonObject json = new Gson().fromJson(Web.get(endpoint, null), JsonObject.class);
			ExclusionStrategy strategy = new ExclusionStrategy() {
				@Override
				public boolean shouldSkipClass(Class<?> clazz) {
					return false;
				}
				@Override
				public boolean shouldSkipField(FieldAttributes field) {
					return field.getAnnotation(Mod.Exclude.class) != null;
				}
			};
			List<Mod> mods = new GsonBuilder().addDeserializationExclusionStrategy(strategy).addSerializationExclusionStrategy(strategy).create().fromJson(json.get("mods").getAsJsonArray(), new TypeToken<List<Mod>>(){}.getType());
			mods.forEach(m -> {
				cachedMods.put(m.getId(), m);
				m.init();
			});
			callback.accept(mods);
		} catch (Exception ex) {
			ex.printStackTrace();
			callback.accept(new ArrayList<>());
		}
	}

	public static void removeDeprecated() {
		new Thread(() -> {
			Thread.currentThread().setName("Mod deprecation checker thread");
			String EMCDir = OSUtils.getMCDir() + "libraries" + File.separator + "EMC" + File.separator + IMinecraft.getMinecraftVersion() + File.separator;
			String[] remove = new String[]{"fabric-api.jar", "Baritone.jar", "multiconnect.jar", "SeedCracker.jar"};
			for (String f : remove) {
				File file = new File(EMCDir + f);
				if (file.exists()) {
					Main.logger.info("Removing deprecated mod {}", file.getName());
					file = new File(EMCDir + f + ".delete");
					if (!file.exists()) {
						try {
							FileUtils.writeStringToFile(file, "Delete mod", "UTF-8");
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		}).start();
	}

}
