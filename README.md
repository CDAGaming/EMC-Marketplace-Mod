The EMC-Marketplace plugin
===================

This is the EMC plugin for the [EMC framework](https://gitlab.com/EMC-Framework/EMC) marketplace.

Demo
-------------------
![Installing a mod](https://gitlab.com/EMC-Framework/EMC-Marketplace-Mod/raw/master/res/demo.jpg)

License
-------------------

The EMC-Marketplace plugin is licensed under the MIT license
